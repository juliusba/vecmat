package vecmat

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
)

func ZeroVec(dimensions int) []float64 {
	return make([]float64, dimensions)
}

func OnesVec(dimensions int) []float64 {
	ones := make([]float64, dimensions)
	for i := 0; i < dimensions; i++ {
		ones[i] = 1.0
	}
	return ones
}

func XVec(dimensions int, X float64) []float64 {
	xvec := make([]float64, dimensions)
	for i := 0; i < dimensions; i++ {
		xvec[i] = X
	}
	return xvec
}

func RandVec(dimensions int) []float64 {
	vector := make([]float64, dimensions)
	for i := 0; i < dimensions; i++ {
		vector[i] = rand.Float64()
		if rand.Intn(2) == 1 {
			vector[i] *= -1.0
		}
	}
	return vector
}

func Multiply(vector []float64, factor float64) {
	for i := 0; i < len(vector); i++ {
		vector[i] *= factor
	}
}

// Does not alter original.
func MultiplyNew(vector []float64, factor float64) []float64 {
	result := make([]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		result[i] = vector[i] * factor
	}
	return result
}

func Negate(vector []float64) {
	for i := 0; i < len(vector); i++ {
		vector[i] *= -1
	}
}

// Does not alter original.
func NegateNew(vector []float64) []float64 {
	result := make([]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		result[i] = vector[i] * -1
	}
	return result
}

func Add(vector []float64, addend float64) {
	for i := 0; i < len(vector); i++ {
		vector[i] += addend
	}
}

// Does not alter original.
func AddNew(vector []float64, addend float64) []float64 {
	sumVec := make([]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		sumVec[i] = vector[i] + addend
	}
	return sumVec
}

func Scalar(a, b []float64) (scalar float64) {
	if len(a) != len(b) {
		fmt.Println("vecmat.Scalar: vector a and b have different lengths!")
	}
	for i := 0; i < len(a); i++ {
		scalar += a[i] * b[i]
	}
	return
}

func Len(vector []float64) (length float64) {
	for _, component := range vector {
		length += component * component
	}
	length = math.Sqrt(length)
	return
}

func ManhattanLen(vector []float64) (length float64) {
	for _, component := range vector {
		length += math.Abs(component)
	}
	return
}

func Norm(vector []float64) (normalized []float64, lenght float64) {
	normalized = make([]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		lenght += vector[i] * vector[i]
	}
	lenght = math.Sqrt(lenght)
	for i := 0; i < len(vector); i++ {
		normalized[i] = vector[i] / lenght
	}
	return
}

func Translate(vector []float64, translation []float64) []float64 {
	translated := make([]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		translated[i] = vector[i] + translation[i]
	}

	return translated
}

func Rotate(vector []float64, rm *Matrix) []float64 {
	rotated := make([]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		rotated[i] = Scalar(vector, rm.Row(i))
	}

	return rotated
}

func Invert(vector []float64) []float64 {
	inverted := make([]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		inverted[i] = vector[i] * -1
	}

	return inverted
}

func Distance(vector1, vector2 []float64) float64 {
	distance := 0.0
	for i := 0; i < len(vector1); i++ {
		d := vector1[i] - vector2[i]
		distance += d * d
	}

	distance = math.Sqrt(distance)
	return distance
}

func DistanceManhattan(vector1, vector2 []float64) float64 {
	distance := 0.0
	for i := 0; i < len(vector1); i++ {
		d := math.Abs(vector1[i] - vector2[i])
		distance += d
	}

	return distance
}

func DistanceUnit(vector1, vector2, unitVector []float64) float64 {
	distance := 0.0
	for i := 0; i < len(vector1); i++ {
		unitDist := (vector1[i] - vector2[i]) / unitVector[i]
		distance += unitDist * unitDist
	}

	distance = math.Sqrt(distance)
	return distance
}

func DistanceUnitManhattan(vector1, vector2, unitVector []float64) float64 {
	distance := 0.0
	for i := 0; i < len(vector1); i++ {
		d := math.Abs(vector1[i] - vector2[i])
		if unitVector[i] == 0 {
			if d != 0 {
				panic(errors.New("unitVector contains a zero!"))
			}
			continue
		}
		distance += d / unitVector[i]
	}

	return distance
}

// -----------------------------------------------------------------------------------------------
// --------------------------- Vector disguised as matrix  ---------------------------------------
// -----------------------------------------------------------------------------------------------

func (this *Matrix) Len() float64 {
	if !this.IsVector() {
		fmt.Println("ERROR (VecMat.Matrix.Len): Trying to take norm of non-vector!")
	}

	length := 0.0
	for _, component := range this.array {
		length += component * component
	}
	length = math.Sqrt(length)

	return length
}

func (this *Matrix) Norm(length float64) {
	if !this.IsVector() {
		fmt.Println("ERROR (VecMat.Matrix.Norm): Trying to take norm of non-vector!")
	}

	for i := 0; i < len(this.array); i++ {
		this.array[i] /= length
	}
}
