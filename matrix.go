package vecmat

import (
	"fmt"
	"log"
	"math"
	"math/rand"
)

type Matrix struct {
	array      []float64
	rows, cols int
}

func NewMatrix(array []float64, rows, cols int) (m *Matrix) {
	m = new(Matrix)
	m.array = array
	m.rows = rows
	m.cols = cols
	return
}

func Zeros(rows, cols int) *Matrix {
	mat := new(Matrix)
	mat.rows = rows
	mat.cols = cols
	mat.array = make([]float64, rows*cols)
	return mat
}

func Eye(dimensions int) *Matrix {
	eye := Zeros(dimensions, dimensions)
	for i := 0; i < dimensions; i++ {
		eye.array[i*dimensions+i] = 1.0
	}
	return eye
}

func Rand(rows, cols int) *Matrix {
	randMatrix := Zeros(rows, cols)
	randMatrix.DoAll(func(row, col int, val *float64) {
		*val = rand.Float64()
		if rand.Intn(1) == 0 {
			*val *= -1.0
		}
	})
	return randMatrix
}

func VecToMat(vector []float64) (m *Matrix) {
	m = new(Matrix)
	m.array = vector
	m.rows = 1
	m.cols = len(vector)
	return m
}

func (m *Matrix) Get(row, col int) float64 {
	return m.array[row*m.cols+col]
}

func (m *Matrix) Set(row, col int, val float64) {
	m.array[row*m.cols+col] = val
}

func (m *Matrix) Row(row int) []float64 {
	r := make([]float64, m.cols)

	index := row * m.cols
	for col := 0; col < m.cols; col++ {
		r[col] = m.array[index]
		index++
	}
	return r
}

func (m *Matrix) Col(col int) []float64 {
	c := make([]float64, m.rows)
	index := col
	for row := 0; row < m.rows; row++ {
		c[row] = m.array[index]
		index += m.cols
	}
	return c
}

func (m *Matrix) DoAll(fn func(row, col int, val *float64)) {
	index := 0
	for row := 0; row < m.rows; row++ {
		for col := 0; col < m.cols; col++ {
			fn(row, col, &m.array[index])
			index++
		}
	}
}

func (m *Matrix) DoRow(row int, fn func(col int, val *float64)) {
	index := row * m.cols
	for col := 0; col < m.cols; col++ {
		fn(col, &m.array[index])
		index++
	}
}

func (m *Matrix) DoCol(col int, fn func(row int, val *float64)) {
	index := col
	for row := 0; row < m.rows; row++ {
		fn(col, &m.array[index])
		index += m.cols
	}
}

func (m *Matrix) IsVector() bool {
	if m.rows != 1 && m.cols != 1 {
		return false
	}
	return true
}

func (m *Matrix) Multiply(factor float64) {
	for i := 0; i < len(m.array); i++ {
		m.array[i] *= factor
	}
}

func (m *Matrix) Divide(dividend float64) {
	for i := 0; i < len(m.array); i++ {
		m.array[i] /= dividend
	}
}

func (m *Matrix) Translate(translation *Matrix) {
	if !m.IsVector() {
		log.Fatal("ERROR (VecMat.Matrix.Translate): Trying to translate a non-vector!")
	} else if !translation.IsVector() {
		log.Fatal("ERROR (VecMat.Matrix.Translate): Trying to translate by non-vector!")
	}

	for i := 0; i < len(m.array); i++ {
		m.array[i] += translation.array[i]
	}
}

func (m *Matrix) NormalizeRow(row int) {
	length := 0.0

	index := row * m.cols
	for col := 0; col < m.cols; col++ {
		length += m.array[index] * m.array[index]
		index++
	}
	length = math.Sqrt(length)

	index -= m.cols
	for col := 0; col < m.cols; col++ {
		m.array[index] /= length
		index++
	}
}

func (m *Matrix) NormalizeCol(col int) {
	length := 0.0

	index := col
	for row := 0; row < m.rows; row++ {
		length += m.array[index] * m.array[index]
		index += m.cols
	}
	length = math.Sqrt(length)

	index = col
	for row := 0; row < m.rows; row++ {
		m.array[index] /= length
		index += m.cols
	}
}

func (m *Matrix) TranslateRow(row int, translation []float64) {
	index := row * m.cols
	for col := 0; col < m.cols; col++ {
		m.array[index] += translation[col]
		index++
	}
}

func (m *Matrix) TranslateCol(col int, translation []float64) {
	index := col
	for row := 0; row < m.rows; row++ {
		m.array[index] += translation[row]
		index += m.cols
	}
}

func (m *Matrix) String() (str string) {
	str = "+"
	for c := 0; c < m.cols; c++ {
		str = fmt.Sprint(str, "----------")
	}
	str = fmt.Sprint(str, "+")
	index := 0
	for r := 0; r < m.rows; r++ {
		str = fmt.Sprint(str, "\n|")
		for c := 0; c < m.cols; c++ {
			str = fmt.Sprintf("%s %.2e ", str, m.array[index])
			index++
		}
		str = fmt.Sprint(str, "|")
	}
	str = fmt.Sprint(str, "\n+")
	for c := 0; c < m.cols; c++ {
		str = fmt.Sprint(str, "----------")
	}
	str = fmt.Sprint(str, "+\n")
	return
}

// --------------------------------------------------------------------------------------------------
// ------------------------------------- Creation methods -------------------------------------------
// --------------------------------------------------------------------------------------------------

func Dot(a, b *Matrix) *Matrix {
	rows := a.rows
	cols := b.cols
	dot := Zeros(rows, cols)

	dot.DoAll(func(row, col int, val *float64) {
		*val = InnerScalar(a, b, row, col)
	})
	return dot
}

func InnerScalar(a, b *Matrix, rowA, colB int) float64 {
	if a.cols != b.rows {
		log.Fatal("ERROR (VecMat.InnerScalar): Trying to take scalar of non-equal sized vectors!")
	}

	indexA := rowA * a.cols
	indexB := colB

	scalar := 0.0
	for i := 0; i < a.cols; i++ {
		scalar += a.array[indexA] * b.array[indexB]

		indexA++
		indexB += b.cols
	}
	return scalar
}

func InnerRowScalar(a, b *Matrix, rowA, rowB int) float64 {
	if a.cols != b.cols {
		log.Fatal("ERROR (VecMat.InnerRowScalar): Trying to take scalar of non-equal sized vectors!")
	}

	indexA := rowA * a.cols
	indexB := rowB * b.cols

	scalar := 0.0
	for i := 0; i < a.cols; i++ {
		scalar += a.array[indexA] * b.array[indexB]

		indexA++
		indexB++
	}
	return scalar
}

func InnerColScalar(a, b *Matrix, colA, colB int) float64 {
	if a.rows != b.rows {
		log.Fatal("ERROR (VecMat.InnerColScalar): Trying to take scalar of non-equal sized vectors!")
	}

	indexA := colA
	indexB := colB

	scalar := 0.0
	for i := 0; i < a.cols; i++ {
		scalar += a.array[indexA] * b.array[indexB]

		indexA += a.cols
		indexB += b.cols
	}
	return scalar
}

func Transpose(m *Matrix) *Matrix {
	transpose := Zeros(m.cols, m.rows)

	for i := 0; i < m.rows; i++ {
		for j := 0; j < m.cols; j++ {
			transpose.array[j*transpose.cols+i] = m.array[i*m.cols+j]
		}
	}
	return transpose
}

func SumRows(m *Matrix) []float64 {
	sum := make([]float64, m.cols)
	index := 0
	for row := 0; row < m.rows; row++ {
		for col := 0; col < m.cols; col++ {
			sum[col] += m.array[index]
			index++
		}
	}
	return sum
}
