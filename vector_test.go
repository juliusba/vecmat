package vecmat

import (
	"fmt"
	"math"
	"testing"
)

func TestScalar(t *testing.T) {
	a := []float64{
		1, 2, 3,
	}
	b := []float64{
		1, 2, 3,
	}
	if Scalar(a, b) != 1+4+9 {
		t.Fail()
	}

	dimensions := 25
	eye := Eye(dimensions)
	for i := 0; i < dimensions; i++ {
		for j := 0; j < i; j++ {
			if scalar := InnerRowScalar(eye, eye, i, j); scalar != 0.0 {
				fmt.Println("error:", scalar)
			}
		}
	}
}

func TestNorm(t *testing.T) {
	vector := []float64{
		3.0, 1.0, 2.0,
	}

	norm, lenght := Norm(vector)

	if lenght != math.Sqrt(14) || norm[0] != 3.0/math.Sqrt(14) || norm[1] != 1.0/math.Sqrt(14) || norm[2] != 2.0/math.Sqrt(14) {
		fmt.Println(norm)
		t.Fail()
	}
}
