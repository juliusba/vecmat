package raw

import (
	"testing"
	"vecmat"
)

func TestCol(t *testing.T) {
	matrix := [][]float64{
		[]float64{11, 12, 13},
		[]float64{21, 22, 23},
		[]float64{31, 32, 33},
		[]float64{41, 42, 43},
	}

	col := []float64{
		12, 22, 32, 42,
	}

	result := Col(matrix, 1)

	for i := 0; i < len(col); i++ {
		if col[i] != result[i] {
			t.Fail()
		}
	}
}

func TestEye(t *testing.T) {
	eye := [][]float64{
		[]float64{1, 0, 0},
		[]float64{0, 1, 0},
		[]float64{0, 0, 1},
	}

	result := Eye(3)

	for i := 0; i < len(eye); i++ {
		for j := 0; j < len(eye); j++ {
			if eye[i][j] != result[i][j] {
				t.Fail()
			}
		}
	}
}

func TestTranspose(t *testing.T) {
	matrix := [][]float64{
		[]float64{11, 12, 13},
		[]float64{21, 22, 23},
		[]float64{31, 32, 33},
		[]float64{41, 42, 43},
	}

	transpose := [][]float64{
		[]float64{11, 21, 31, 41},
		[]float64{12, 22, 32, 42},
		[]float64{13, 23, 33, 43},
	}

	result := Transpose(matrix)

	for i := 0; i < len(transpose); i++ {
		for j := 0; j < len(transpose[0]); j++ {
			if transpose[i][j] != result[i][j] {
				t.Fail()
			}
		}
	}
}

func TestDot(t *testing.T) {
	a := [][]float64{
		[]float64{1, 2, 3},
		[]float64{2, 4, 6},
		[]float64{3, 6, 9},
		[]float64{4, 8, 12},
	}
	ta := Transpose(a)

	dot := [][]float64{
		[]float64{vecmat.Scalar(a[0], Col(ta, 0)), vecmat.Scalar(a[0], Col(ta, 1)), vecmat.Scalar(a[0], Col(ta, 2)), vecmat.Scalar(a[0], Col(ta, 3))},
		[]float64{vecmat.Scalar(a[1], Col(ta, 0)), vecmat.Scalar(a[1], Col(ta, 1)), vecmat.Scalar(a[1], Col(ta, 2)), vecmat.Scalar(a[1], Col(ta, 3))},
		[]float64{vecmat.Scalar(a[2], Col(ta, 0)), vecmat.Scalar(a[2], Col(ta, 1)), vecmat.Scalar(a[2], Col(ta, 2)), vecmat.Scalar(a[2], Col(ta, 3))},
		[]float64{vecmat.Scalar(a[3], Col(ta, 0)), vecmat.Scalar(a[3], Col(ta, 1)), vecmat.Scalar(a[3], Col(ta, 2)), vecmat.Scalar(a[3], Col(ta, 3))},
	}

	result := Dot(a, ta)

	for i := 0; i < len(dot); i++ {
		for j := 0; j < len(dot[0]); j++ {
			if dot[i][j] != result[i][j] {
				t.Fail()
			}
		}
	}
}
