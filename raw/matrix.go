package raw

import (
	"math"
	"vecmat"
)

func IsVector(vector []float64) bool {
	for i := 0; i < len(vector); i++ {
		if math.IsNaN(vector[i]) {
			return false
		}
	}

	return true
}

// For testing purposes. Returns true if the array satisfies the conditions that define a matrix.
func IsMatrix(matrix [][]float64) bool {
	cols := len(matrix[0])
	for i := 1; i < len(matrix); i++ {
		if len(matrix[i]) != cols {
			return false
		} else if !IsVector(matrix[i]) {
			return false
		}
	}

	return true
}

func MatToVec(matrix [][]float64) []float64 {
	return Col(matrix, 0)
}

func VecToMat(vector []float64) [][]float64 {
	matrix := make([][]float64, len(vector))
	for i := 0; i < len(vector); i++ {
		matrix[i] = []float64{vector[i]}
	}
	return matrix
}

func Dot(a, b [][]float64) [][]float64 {
	n := len(a)
	p := len(b[0])

	dot := make([][]float64, n)
	for i := 0; i < n; i++ {
		dot[i] = make([]float64, p)
		for j := 0; j < p; j++ {
			dot[i][j] = vecmat.Scalar(a[i], Col(b, j))
		}
	}
	return dot
}

func Col(matrix [][]float64, index int) []float64 {
	col := make([]float64, len(matrix))
	for i := 0; i < len(matrix); i++ {
		col[i] = matrix[i][index]
	}
	return col
}

func Eye(dimensions int) [][]float64 {
	eye := make([][]float64, dimensions)
	for i := 0; i < dimensions; i++ {
		eye[i] = make([]float64, dimensions)
		eye[i][i] = 1.0
	}
	return eye
}

func Transpose(matrix [][]float64) [][]float64 {
	rows := len(matrix)
	cols := len(matrix[0])

	t := make([][]float64, cols)

	for i := 0; i < cols; i++ {
		t[i] = make([]float64, rows)
		for j := 0; j < rows; j++ {
			t[i][j] = matrix[j][i]
		}
	}

	return t
}
