package raw

import (
	"log"
	"math"
	"vecmat"
)

func Rotate(rotMatrix [][]float64, vector []float64) []float64 {
	if len(vector) != len(rotMatrix) {
		log.Fatal("vec.RotateVector: vector and rotMatrix have different lengths!")
	}

	vm := VecToMat(vector)
	vm = Dot(rotMatrix, vm)

	return Col(vm, 0)
}

func RandomRotationMatrix(dimensions int) [][]float64 {
	rm := make([][]float64, dimensions)
	for i := 0; i < dimensions; i++ {
		rm[i] = vecmat.RandVec(dimensions)
	}

	// Precision increases with the number of repetitions (t < reps)
	for t := 0; t < 5; t++ {
		for i := 0; i < dimensions; i++ {
			for j := 0; j < i; j++ {
				vecmat.Translate(rm[i], vecmat.MultiplyNew(rm[j], -vecmat.Scalar(rm[i], rm[j])))
			}
			vecmat.Norm(rm[i])
		}
	}

	return rm
}

func RotationMatrix(vector, target []float64) [][]float64 {
	// Roation matrix from one vector to another target vector.

	// The solution is not unique as any additional rotation perpendicular to
	// the target vector will also yield a solution)

	// However, the output is deterministic.

	R1 := RotationMatrixToPole(target)
	R2 := RotationMatrixToPole(vector)

	return Dot(Transpose(R1), R2)
}

func RotationMatrixToPole(target []float64) [][]float64 {
	// Rotate to 1,0,0...
	n := len(target)

	working := make([][]float64, len(target))
	tm := make([][]float64, len(target))
	for i := 0; i < len(target); i++ {
		tm[i] = []float64{target[i]}
		working[i] = []float64{target[i]}
	}

	rm := Eye(n)

	for i := 0; i < n; i++ {
		angle := math.Atan2(working[0][0], working[i][0])
		rotMaIn := rotationMatrixInds(angle, n, 0, i)
		rm = Dot(rotMaIn, rm)
		working = Dot(rm, tm)
	}

	return rm
}

func rotationMatrixInds(angle float64, n, ax1, ax2 int) [][]float64 {
	//'n'-dimensional rotation matrix 'angle' radians in coordinate plane with indices 'ax1' and 'ax2'

	s := math.Sin(angle)
	c := math.Cos(angle)

	i := Eye(n)

	i[ax1][ax1] = s
	i[ax1][ax2] = c
	i[ax2][ax1] = c
	i[ax2][ax2] = -s

	return i
}
