package raw

import (
	"fmt"
	"math"
	"testing"
	"vecmat"
)

func TestRandomRotationMatrix(t *testing.T) {
	fmt.Println("TestRandomRotationMatrix...")
	dimensions := 10

	eye := Eye(dimensions)
	rm := RandomRotationMatrix(dimensions)
	eye_rot := make([][]float64, dimensions)
	for j := 0; j < dimensions; j++ {
		eye_rot[j] = Rotate(rm, eye[j])
	}

	maxError := 0.0
	avgError := 0.0
	for j := 0; j < dimensions; j++ {
		for k := 0; k < j; k++ {
			err := math.Abs(vecmat.Scalar(eye_rot[j], eye_rot[k]))
			maxError = math.Max(maxError, err)
			avgError += err
			if err > math.Pow(10, -15) {
				t.Fail()
			}
		}
	}
	avgError /= float64(dimensions * (dimensions - 1) / 2)
	fmt.Println("avgError:", avgError, " maxError:", maxError)
	fmt.Println()
}

func TestRotationToPole(t *testing.T) {

	fmt.Println("TestRotationToPole... ")

	dimensions := 10
	v := vecmat.RandVec(dimensions)
	rm := RotationMatrixToPole(v)
	vrot := Rotate(rm, v)
	v_length := vecmat.Norm(v)
	vrot_length := vecmat.Norm(vrot)

	fmt.Println("Length v:", v_length, "vrot:", vrot_length)
	fmt.Println()
}

func TestRotation(t *testing.T) {

	fmt.Println("TestRotationToPole... ")

	dimensions := 10
	v := vecmat.RandVec(dimensions)
	ta := vecmat.RandVec(dimensions)

	rm := RotationMatrix(v, ta)
	vrot := Rotate(rm, v)

	cosang := vecmat.Scalar(ta, vrot) / math.Sqrt(vecmat.Scalar(vrot, vrot)*vecmat.Scalar(ta, ta))

	len_v := vecmat.Norm(v)
	len_vrot := vecmat.Norm(vrot)
	fmt.Println("length of rotated relative to original:", len_v/len_vrot)

	ang := 0.0
	if cosang > 1 {
		fmt.Println("Rounding error of", cosang-1)
	} else {
		ang = math.Acos(cosang)
	}

	fmt.Println("cos angular difference:", cosang, ",", ang, "radians")
	fmt.Println()
}

// http://jellymatter.com/2013/09/25/rotation-matrix-from-one-vector-to-another-in-n-dimensions/
