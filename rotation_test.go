package vecmat

import (
	"fmt"
	"math"
	"testing"
)

func TestRandomRotationMatrix(t *testing.T) {
	fmt.Println("RandomRotationMatrix...")
	dimensions := 10

	eye := Eye(dimensions)
	rm := RandomRotationMatrix(dimensions)
	eye_rot_vecs := make([][]float64, dimensions)
	for j := 0; j < dimensions; j++ {
		eye_rot_vecs[j] = Rotate(eye.Row(j), rm)
	}

	maxError := 0.0
	avgError := 0.0
	for j := 0; j < dimensions; j++ {
		for k := 0; k < j; k++ {
			err := math.Abs(Scalar(eye_rot_vecs[j], eye_rot_vecs[k]))
			maxError = math.Max(maxError, err)
			avgError += err
			if err > math.Pow(10, -15) {
				t.Fail()
			}
		}
	}
	avgError /= float64(dimensions * (dimensions - 1) / 2)
	fmt.Println("avgError:", avgError, " maxError:", maxError)
	fmt.Println()
}

func TestRotationToPole(t *testing.T) {

	fmt.Println("TestRotationToPole... ")

	for i := 1; i < 5; i++ {
		v := RandVec(i * 10)
		rm := RotationMatrixToPole(v)
		vrot := Rotate(v, rm)
		v, v_length := Norm(v)
		vrot, vrot_length := Norm(vrot)

		fmt.Println("Length v:", v_length)
		fmt.Println("Length vrot:", vrot_length)
	}

	fmt.Print("Pass!\n\n")
}

func TestRotation(t *testing.T) {

	fmt.Println("TestRotationToPole... ")

	for i := 1; i < 5; i++ {
		v := RandVec(i * 10)
		t := RandVec(i * 10)

		rm := RotationMatrix(v, t)
		vrot := Rotate(v, rm)

		cosang := Scalar(t, vrot) / math.Sqrt(Scalar(vrot, vrot)*Scalar(t, t))

		v, len_v := Norm(v)
		vrot, len_vrot := Norm(vrot)
		fmt.Println("length of rotated relative to original:", len_v/len_vrot)

		ang := 0.0
		if cosang > 1 {
			fmt.Println("Rounding error of", cosang-1)
		} else {
			ang = math.Acos(cosang)
		}

		fmt.Println("cos angular difference:", cosang, ",", ang, "radians")
	}

	fmt.Println("Pass\n\n")
}

// http://jellymatter.com/2013/09/25/rotation-matrix-from-one-vector-to-another-in-n-dimensions/
