package vecmat

import (
	"fmt"
	"strconv"
	"testing"
)

func TestCol(t *testing.T) {
	rows, cols := 4, 3

	matrix := Zeros(rows, cols)
	matrix.DoAll(func(row, col int, val *float64) {
		s := strconv.Itoa(row + 1)
		s += strconv.Itoa(col + 1)
		*val, _ = strconv.ParseFloat(s, 64)
	})

	col := []float64{
		12, 22, 32, 42,
	}

	result := matrix.Col(1)

	for i := 0; i < len(col); i++ {
		if col[i] != result[i] {
			t.Fail()
		}
	}
}

func TestEye(t *testing.T) {
	eye := Zeros(4, 4)
	eye.array = []float64{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}

	result := Eye(4)

	result.DoAll(func(row, col int, val *float64) {
		if *val != eye.Get(row, col) {
			t.Fail()
		}
	})
}

func TestTranspose(t *testing.T) {
	matrix := Zeros(4, 3)
	matrix.array = []float64{
		11, 12, 13,
		21, 22, 23,
		31, 32, 33,
		41, 42, 43,
	}

	transpose := Zeros(3, 4)
	transpose.array = []float64{
		11, 21, 31, 41,
		12, 22, 32, 42,
		13, 23, 33, 43,
	}

	result := Transpose(matrix)

	transpose.DoAll(func(row, col int, val *float64) {
		if *val != result.Get(row, col) {
			fmt.Println(*val, result.Get(row, col))
			t.Fail()
		}
	})
}

func TestDot(t *testing.T) {
	a := Zeros(3, 3)
	a.DoAll(func(row, col int, val *float64) {
		*val = float64(row*3 + col)
	})
	ta := Transpose(a)

	dot := Zeros(3, 3)
	dot.array = []float64{
		1*1 + 4*2 + 7*3,
		1*4 + 4*5 + 7*6,
		1*7 + 4*8 + 7*9,

		2*1 + 5*2 + 8*3,
		2*4 + 5*5 + 8*6,
		2*7 + 5*8 + 8*9,

		3*1 + 6*2 + 9*3,
		3*4 + 6*5 + 9*6,
		3*7 + 6*8 + 9*9,
	}

	res := Dot(a, ta)

	res.DoAll(func(row, col int, val *float64) {
		if *val != res.Get(row, col) {
			t.Fail()
		}
	})
}
