package vecmat

type Vector2D struct {
	X, Y float64
}

func NewVector2D(x, y float64) *Vector2D {
	vec := new(Vector2D)
	vec.X = x
	vec.Y = y

	return vec
}
