package vecmat

import "math"

func RandomRotationMatrix(dimensions int) *Matrix {
	rm := Rand(dimensions, dimensions)

	// Precision increases with the number of repetitions (t < reps)
	for t := 0; t < 5; t++ {
		for i := 0; i < dimensions; i++ {
			for j := 0; j < i; j++ {
				scalar := InnerRowScalar(rm, rm, i, j)
				translation := rm.Row(j)
				Multiply(translation, -scalar)
				rm.TranslateRow(i, translation)
			}
			rm.NormalizeRow(i)
		}
	}

	return rm
}

func RotationMatrix(vector, target []float64) *Matrix {
	// Roation matrix from one vector to another target vector.

	// The solution is not unique as any additional rotation perpendicular to
	// the target vector will also yield a solution)

	// However, the output is deterministic.

	R1 := RotationMatrixToPole(target)
	R2 := RotationMatrixToPole(vector)
	R1T := Transpose(R1)

	return Dot(R1T, R2)
}

func RotationMatrixToPole(target []float64) *Matrix {
	// Rotate to 1,0,0...
	n := len(target)

	working := Zeros(1, len(target))
	tm := Zeros(1, len(target))
	copy(working.array, target)
	copy(tm.array, target)

	rm := Eye(n)

	for i := 0; i < n; i++ {
		angle := math.Atan2(working.array[0], working.array[i])
		rotMaIn := rotationMatrixInds(angle, n, 0, i)
		rm = Dot(rotMaIn, rm)
		working = Dot(rm, Transpose(tm))
	}

	return rm
}

func rotationMatrixInds(angle float64, n, ax1, ax2 int) *Matrix {
	//'n'-dimensional rotation matrix 'angle' radians in coordinate plane with indices 'ax1' and 'ax2'

	s := math.Sin(angle)
	c := math.Cos(angle)

	m := Eye(n)

	m.array[ax1*n+ax1] = s
	m.array[ax1*n+ax2] = c
	m.array[ax2*n+ax1] = c
	m.array[ax2*n+ax2] = -s

	return m
}
